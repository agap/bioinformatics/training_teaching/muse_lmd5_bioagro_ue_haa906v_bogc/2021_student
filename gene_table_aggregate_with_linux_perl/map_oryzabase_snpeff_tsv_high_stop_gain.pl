#!/usr/bin/perl

use strict;
use Data::Dumper;
use Getopt::Long;

my $usage = qq~Usage:$0 <args> [<opts>]
where <args> are:
    -l, --lst          <Locus list to be parsed>
    -s, --syn          <Correspondence file>
    -o, --out          <Correspondence locus list>
~;
$usage .= "\n";

my ($lst,$lst2,$output);


GetOptions(
    "lst=s" => \$lst,
    "syn=s" => \$lst2,
    "out=s" => \$output
);


die $usage
  if ( !$lst || !$lst2 ||!$output);
  
my %correspondence;



##############################################################
# parse GBS read count list file
##############################################################

open(my $LST,$lst);
open(my $LST2,$lst2);
open(my $OUT,">$output");
while(<$LST2>)
{
	my $line = $_;
	chomp($line);
	my @tab = split (/\t/, $line);

	print  $tab[0] . " " . $tab[4] . " $tab[21]\n";
	$correspondence{$tab[0]} = $tab[4] . "," . $tab[21];

}
while(<$LST>)
{
	my $line = $_;
	chomp ($line);
	my @tab = split (/\t/, $line);
	if(exists $correspondence{$tab[0]})
	{
		#print $OUT $correspondence{$tab[0]} . "\t" . $tab[1] . "\n";
		print $OUT $line . "\t" . $correspondence{$tab[0]} . "\n";
	}
	else
	{
		#print "Scaffold<2kb\t" . $line . "\n";
		print $OUT $line . "\tNoImpactHIGH_stop-gain\n";
	}
}
