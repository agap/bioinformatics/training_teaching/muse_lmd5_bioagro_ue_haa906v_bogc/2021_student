#!/usr/bin/perl

use strict;
use Data::Dumper;
use Getopt::Long;

my $usage = qq~Usage:$0 <args> [<opts>]
where <args> are:
    -l, --lst          <Locus list to be parsed>
    -s, --syn          <Correspondence file>
    -o, --out          <Correspondence locus list>
~;
$usage .= "\n";

my ($lst,$lst2,$output);


GetOptions(
    "lst=s" => \$lst,
    "syn=s" => \$lst2,
    "out=s" => \$output
);


die $usage
  if ( !$lst || !$lst2 ||!$output);
  
my %correspondence;



##############################################################
# parse GBS read count list file
##############################################################

open(my $LST,$lst);
open(my $LST2,$lst2);
open(my $OUT,">$output");
while(<$LST2>)
{
	my $line = $_;
	chomp($line);
	my @tab = split (/\t/, $line);
	my $gene;
	if($tab[2] eq "mRNA")
	{
		if($tab[8] =~ /ID=(LOC_Os.+?g.+?);/) #ID=LOC_Os01g01010.1;
		#if($tab[8] =~ /Parent=(Os.+?g.+?);/) #Name=Os01g0100200;
		#if($tab[8] =~ /Parent=(Os\d\dg\d\d\d\d\d\d\d);/)
		{
			$gene=$1;
			if($gene =~ /\.1$/)
			{
				$gene=~ s/\.1//;
				$correspondence{$gene}{"c"} = $tab[0];
				$correspondence{$gene}{"b"} = $tab[3];
				$correspondence{$gene}{"e"} = $tab[4];
				$correspondence{$gene}{"l"} = $tab[4] - $tab[3] + 1;
				
				if($tab[8] =~ /Dbxref=(.+?);/)
				{
					print  $gene . " " . $1 . "\n";
					$correspondence{$gene}{"db"} = $1;
				}
			}
		}
	}
}
while(<$LST>)
{
	my $line = $_;
	chomp ($line);
	my @tab = split (/\t/, $line);
	if(exists $correspondence{$tab[1]})
	{
		if(exists $correspondence{$tab[1]}{"db"})
		{
			#print $OUT $correspondence{$tab[0]} . "\t" . $tab[1] . "\n";
			print $OUT $line . "\t" . $correspondence{$tab[1]}{"c"} ."\t" . $correspondence{$tab[1]}{"b"} ."\t" . $correspondence{$tab[1]}{"e"} ."\t" .  $correspondence{$tab[1]}{"l"} ."\t" . $correspondence{$tab[1]}{"db"} . "\n";
		}
		else
		{
			#print "Scaffold<2kb\t" . $line . "\n";
			print $OUT $line . "\t" . $correspondence{$tab[1]}{"c"} ."\t" . $correspondence{$tab[1]}{"b"} ."\t" . $correspondence{$tab[1]}{"e"} .  "\t" . $correspondence{$tab[1]}{"l"} . "\tNoDbxref\n";
		}
	}
	else
	{
		print $OUT $line . "\tMSU_C\tMSU_b\tMSU_e\tMSU_lbp\tNoDbxref\n";
	}

}
