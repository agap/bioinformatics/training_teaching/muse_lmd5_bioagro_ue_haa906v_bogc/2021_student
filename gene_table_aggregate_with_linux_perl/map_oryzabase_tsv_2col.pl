#!/usr/bin/perl

use strict;
use Data::Dumper;
use Getopt::Long;

my $usage = qq~Usage:$0 <args> [<opts>]
where <args> are:
    -l, --lst          <Locus list to be parsed>
    -s, --syn          <Correspondence file>
    -o, --out          <Correspondence locus list>
~;
$usage .= "\n";

my ($lst,$lst2,$output);


GetOptions(
    "lst=s" => \$lst,
    "syn=s" => \$lst2,
    "out=s" => \$output
);


die $usage
  if ( !$lst || !$lst2 ||!$output);
  
my %correspondence;



##############################################################
# parse GBS read count list file
##############################################################

open(my $LST,$lst);
open(my $LST2,$lst2);
open(my $OUT,">$output");
while(<$LST2>)
{
	my $line = $_;
	chomp($line);
	#if($line =~ /(LOC_Os.+?g.+?)\.1\.p.+?\t(.+)/) #Oryza_sativa_Nipponbare_7.0.protein_length.tsv
	if($line =~ /(LOC_Os.+?g.+?)\.1\t(.+)/) #LOC_Os_greenphyl.tsv
	{
		print  $1 . " " . $2 . "\n";
		$correspondence{$1} = $2;
	}

}
while(<$LST>)
{
	my $line = $_;
	chomp ($line);
	my @tab = split (/\t/, $line);
	if(exists $correspondence{$tab[1]})
	{
		#print $OUT $correspondence{$tab[0]} . "\t" . $tab[1] . "\n";
		print $OUT $line . "\t" . $correspondence{$tab[1]} . "\n";
	}
	else
	{
		#print $OUT $line . "\tNoLaa\n";
		print $OUT $line . "\tNoGP\n";
	}
}
