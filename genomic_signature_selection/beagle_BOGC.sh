#!/bin/bash

#SBATCH -o phasing.%N.%j.out
#SBATCH -e phasing.%N.%j.err
#SBATCH --mail-type END
#SBATCH --mail-user=aude.gilabert@cirad.fr
#
#SBATCH --partition long
#SBATCH --cpus-per-task 6
#SBATCH --mem 150GB
#
#SBATCH --job-name="phasing"

module purge
module load java-jdk/8.0.112

if [ ! -f beagle.18May20.d20.jar ]; then
  echo
  echo "Downloading beagle.18May20.d20.jar"
  wget http://faculty.washington.edu/browning/beagle/beagle.18May20.d20.jar
fi

###

java -Xmx50G -jar beagle.18May20.d20.jar gt=japonica_chr7.vcf.gz out=japonica_chr7.vcf.gt
java -Xmx100G -jar beagle.18May20.d20.jar gt=indica_chr7.vcf.gz out=indica_chr7.vcf.gt

echo "--Fin-- !"
